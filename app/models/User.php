<?php

namespace mvc\models;

use mvc\core\Database;

class User
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->database = \mvc\core\Database::getInstance();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * Add user to database
     * @param $user
     */
    public function add()
    {
        $this->database->insertRow('user', "( `name`, `surname`, `email`, `password`) VALUES(?,?,?,?)", [$this->name, $this->surname, $this->email, $this->password]);
    }

    /**
     * Delete user from database
     * @param $user
     */

    public function delete()
    {
        $this->database->deleteRow('user', 'WHERE id = ?', [$this->id]);
    }


    public function load()
    {
        $result = $this->database->getRows('*', 'user');

        return $result;
    }

    public function getUserByEmail()
    {

        $db = new Database();
        $result = $this->database->getRow('*', 'user', 'WHERE email = ?', [$this->email]);

        if ($result !== null) {
            $this->name = $result['name'];
            $this->password = $result['password'];
            $this->id = $result['id'];
        }
    }
}