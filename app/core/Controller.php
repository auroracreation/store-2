<?php

namespace mvc\core;


use mvc\models\User;


class Controller
{
    public function view($view, $data = [])
    {
        require_once '../app/views/' . $view . '.php';
    }

    public function redirect($url)
    {
        header('Location: ' . $url);
        exit;
    }

    public function header()
    {
        $this->view('base/header');
    }

    /**
     * Parse URL into array elements
     * @return array
     */
    public function parseUrl($uri) {
        if (isset($uri)) {
            return $url = explode('/', filter_var(rtrim($uri, '/'), FILTER_SANITIZE_URL));
        }
    }

}
?>