<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\Product as ProductModel;
use mvc\models\Size as SizeModel;
use mvc\models\Category as CategoryModel;
use mvc\models\Colour as ColourModel;

class Product extends Controller
{

    public $product;

    public function __construct()
    {
        $this->product = new ProductModel();
        $this->size = new SizeModel();
        $this->category = new CategoryModel();
        $this->colour = new ColourModel();
    }


    /**
     * add new product
     */

    public function add()
    {
        $this->product->setName($_POST['name']);
        $this->product->setCategory($_POST['categoryoption']);
        $this->product->setSize($_POST['sizeoption']);
        $this->product->setColour($_POST['colouroption']);
        $this->product->setdescription($_POST['description']);
        $this->product->setprice($_POST['price']);
        $this->product->add();
        $this->redirect("Location: http://localhost/mvc/public/product/addproductform");

    }

    public function delete()
    {
        $this->product->setId($_POST['id']);
        $this->product->delete();

        $this->redirect("Location: http://localhost/mvc/public/producttable/viewtable");
    }

    /**
     * Shows products list
     */
    public function showProducts()
    {
        //product load
        $products = $this->product->load();
        //layout render
        $this->view('product/list', $data = ['products' => $products]);
    }

    /**
     * Shows add product form
     */
    public function productform()
    {
        $size = $this->size->load();
        $category = $this->category->load();
        $products = $this->product->load();
        $colour = $this->colour->load();

        $this->view('product/addproductform', $data = ['size' => $size, 'category' => $category, 'product' => $products, 'colour' => $colour]);
    }
    /**
     * Shows  product views
     */
    public function Itemdisplay()
    {
        $category = $this->category->load();
        $product = $this->product->load();

        $this->header();

        $this->view('store/item', $data = ['category' => $category, 'product' => $product]);
    }

}
