<?php

namespace mvc\controllers;

use mvc\core\Controller;
use mvc\models\User;

class Table extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function viewTable()
    {
        $users = $this->user->load();

        $this->view('home/table', $data = ['users' => $users]);
    }
}